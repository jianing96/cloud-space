package com.space.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.space.core.entry.sys.SysUser;
import com.space.core.response.ApiResult;
import com.space.core.utils.AesUtil;
import com.space.core.utils.Constants;
import com.space.core.utils.SecureUtils;
import com.space.mapper.SysUserMapper;
import com.space.core.service.SysUserService;
import com.space.core.params.SysUserQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 张佳宁
 * @description: 系统用户实现方法
 * @return:
 * @date: 2024/3/12 3:46 PM
 */
@Slf4j
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    @Resource
    private SysUserMapper sysUserMapper;

    @Override
    public ApiResult queryUsersByParams(SysUserQuery query) {
        List<SysUser> sysUsers = null;
        SysUser user = null;
        try {
            //获取用户信息
            QueryWrapper<SysUser> userQueryWrapper = new QueryWrapper<>();
            userQueryWrapper.eq("user_name", query.getUserName());
            sysUsers = sysUserMapper.selectList(userQueryWrapper);
            if (sysUsers.size() < 1 || null == sysUsers) {
                return ApiResult.fail("请输入有效的用户名/密码");
            }
            user = sysUsers.get(0);

            String webPas = AesUtil.Decrypt(query.getPassWord(), Constants.PWD_SLAT);
            String userPassword = user.getPassword();
            String salt = Constants.PWD_SLAT;
            String pwd = webPas + salt;
            String dbPassword = SecureUtils.encode(pwd);
            if (!dbPassword.equals(userPassword)) {
                return ApiResult.fail("密码错误，请重新尝试");
            }
            //处理登陆逻辑
            user.setPassword(null);
            StpUtil.setLoginId(user.getId());
            user.setToken(StpUtil.getTokenValue());

        } catch (Exception e) {
            log.info("登陆异常日志打印:{}",e);
            return ApiResult.fail("网络延迟，请刷新重试！");
        }
        return ApiResult.newInstance(user);
    }

//    public static void main(String[] args) throws Exception{
//        ;
//        System.out.println(AesUtil.Encrypt("Hnlh@123456@@",Constants.PWD_SLAT));
//        System.out.println(AesUtil.Decrypt(AesUtil.Encrypt("Hnlh@123456@@",Constants.PWD_SLAT),Constants.PWD_SLAT));;
//    }

}
