package com.space.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.space.core.entry.sys.SysUser;
import org.apache.ibatis.annotations.Mapper;

/** 
 * @description:  
 * @param: null 
 * @return:  
 * @author 张佳宁
 * @date: 2024/3/12 3:40 PM
 */ 
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
    
}

