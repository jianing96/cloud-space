package com.space.utils;

import cn.dev33.satoken.action.SaTokenActionDefaultImpl;
import cn.dev33.satoken.util.SaTokenInsideUtil;
import org.springframework.stereotype.Component;

/**
 * TODO:  获取token
 *
 * @author 张佳宁
 * @version 1.0.0
 * @description: TODO
 * @date 2024/12/12 10:14
 */
@Component
public class TokenUtils extends SaTokenActionDefaultImpl {
    // 重写token生成策略
    @Override
    public String createToken(Object loginId, String loginKey) {
        return SaTokenInsideUtil.getRandomString(32);    // 随机60位字符串
    }


}
