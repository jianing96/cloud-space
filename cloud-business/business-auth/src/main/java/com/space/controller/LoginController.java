package com.space.controller;

import com.space.core.params.SysUserQuery;
import com.space.core.response.ApiResult;
import com.space.core.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 张佳宁
 * @version 1.0.0
 * @description: TODO
 * @date 2024/3/11 2:28 PM
 */
@RestController
@Api(tags = "统一登陆控制类")
@RequestMapping("/login")
public class LoginController {
    @Resource
    private SysUserService sysUserService;


    @PostMapping("/pasLogin")
    @ApiOperation(value = "账号密码登陆", notes = "账号密码登陆")
    public ApiResult loginByUp(@RequestBody SysUserQuery query){
        return sysUserService.queryUsersByParams(query);
    }

}
