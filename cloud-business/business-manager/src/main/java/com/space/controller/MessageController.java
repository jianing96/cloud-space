package com.space.controller;

import com.space.core.entry.p.Pmessage;
import com.space.core.response.ApiResult;
import com.space.service.MessageService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 张佳宁
 * @version 1.0.0
 * @description: TODO
 * @date 2025/1/8 10:31
 */
@RestController
@RequestMapping("/api/messages")
public class MessageController {

    @Resource
    private MessageService messageService;

    @ApiOperation(value = "消息发送", notes = "消息模块")
    @PostMapping("/sendMessage")
    public ApiResult sendMessage(@RequestBody Pmessage message) {
        return ApiResult.newInstance(messageService.save(message));
    }

    @ApiOperation(value = "获取消息列表", notes = "消息模块")
    @PostMapping("/getMessages")
    public ApiResult getMessages() {
        return ApiResult.newInstance(messageService.list());
    }

}
