package com.space.controller;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 张佳宁
 * @version 1.0.0
 * @description: TODO
 * @date 2025/1/7 10:41
 */
public class SignalStrength3DLocalization {

    // 将 RSSI 转换为距离的模型
    public static double rssiToDistance(double rssi, double txPower, double n) {
        return Math.pow(10, (txPower - rssi) / (10 * n));
    }

    // 最小二乘法计算 3D 设备位置
    public static double[] leastSquaresLocalization3D(List<double[]> positions, List<Double> distances) {
        int n = positions.size();
        double[][] A = new double[n - 1][3];
        double[] b = new double[n - 1];

        double[] p1 = positions.get(0);
        double r1 = distances.get(0);

        for (int i = 1; i < n; i++) {
            double[] pi = positions.get(i);
            double ri = distances.get(i);

            A[i - 1][0] = 2 * (pi[0] - p1[0]);
            A[i - 1][1] = 2 * (pi[1] - p1[1]);
            A[i - 1][2] = 2 * (pi[2] - p1[2]);
            b[i - 1] = r1 * r1 - ri * ri - p1[0] * p1[0] - p1[1] * p1[1] - p1[2] * p1[2]
                    + pi[0] * pi[0] + pi[1] * pi[1] + pi[2] * pi[2];
        }

        // 使用矩阵求解线性方程组 A * x = b
        return solveLinearSystem(A, b);
    }

    // 简单的线性方程组求解
    public static double[] solveLinearSystem(double[][] A, double[] b) {
        int n = A.length;
        int m = A[0].length;

        // 转置矩阵 A
        double[][] AT = new double[m][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                AT[j][i] = A[i][j];
            }
        }

        // 计算 ATA 和 ATb
        double[][] ATA = new double[m][m];
        double[] ATb = new double[m];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < m; j++) {
                ATA[i][j] = 0;
                for (int k = 0; k < n; k++) {
                    ATA[i][j] += AT[i][k] * A[k][j];
                }
            }
            ATb[i] = 0;
            for (int k = 0; k < n; k++) {
                ATb[i] += AT[i][k] * b[k];
            }
        }

        // 解 m x m 方程组 ATA * x = ATb
        return gaussianElimination(ATA, ATb);
    }

    // 高斯消元法求解线性方程组
    public static double[] gaussianElimination(double[][] A, double[] b) {
        int n = A.length;

        for (int i = 0; i < n; i++) {
            // 选择主元
            int max = i;
            for (int j = i + 1; j < n; j++) {
                if (Math.abs(A[j][i]) > Math.abs(A[max][i])) {
                    max = j;
                }
            }

            // 交换行
            double[] temp = A[i];
            A[i] = A[max];
            A[max] = temp;

            double t = b[i];
            b[i] = b[max];
            b[max] = t;

            // 消元
            for (int j = i + 1; j < n; j++) {
                double factor = A[j][i] / A[i][i];
                b[j] -= factor * b[i];
                for (int k = i; k < n; k++) {
                    A[j][k] -= factor * A[i][k];
                }
            }
        }

        // 回代求解
        double[] x = new double[n];
        for (int i = n - 1; i >= 0; i--) {
            double sum = 0;
            for (int j = i + 1; j < n; j++) {
                sum += A[i][j] * x[j];
            }
            x[i] = (b[i] - sum) / A[i][i];
        }
        return x;
    }

    public static void main(String[] args) {
        // 信号源的位置 (x, y, z)
        List<double[]> positions = new ArrayList<>();
        positions.add(new double[]{0, 0, 0});   // 信号源1
        positions.add(new double[]{10, 0, 0});  // 信号源2
        positions.add(new double[]{0, 10, 0});  // 信号源3
        positions.add(new double[]{5, 5, 10});  // 信号源4 (在 Z 轴上有高度)

        // 接收到的 RSSI 值
        List<Double> rssis = new ArrayList<>();
        rssis.add(-50.0);
        rssis.add(-55.0);
        rssis.add(-60.0);
        rssis.add(-53.0); // 额外信号源的 RSSI

        // 动态调整的发射功率和环境衰减因子
        double txPower = -40; // 假设的发射功率
        double n = 2.5;       // 动态调整的环境衰减因子

        // 计算到每个信号源的距离
        List<Double> distances = new ArrayList<>();
        for (double rssi : rssis) {
            distances.add(rssiToDistance(rssi, txPower, n));
        }

        // 使用最小二乘法定位设备位置
        double[] position = leastSquaresLocalization3D(positions, distances);

        // 输出设备位置
        System.out.printf("Estimated Position: (%.2f, %.2f, %.2f)%n", position[0], position[1], position[2]);
    }



}
