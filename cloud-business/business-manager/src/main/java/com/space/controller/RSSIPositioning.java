package com.space.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author 张佳宁
 * @version 1.0.0
 * @description: TODO
 * @date 2025/1/7 11:10
 */
public class RSSIPositioning {
    // 信号源的位置 (x, y, z)
    private static final List<double[]> SIGNAL_SOURCE_POSITIONS = new ArrayList<>();

    static {
        // 示例：定义 3 个信号源的位置
        SIGNAL_SOURCE_POSITIONS.add(new double[]{0, 0, 0}); // 信号源 1
        SIGNAL_SOURCE_POSITIONS.add(new double[]{10, 0, 0}); // 信号源 2
        SIGNAL_SOURCE_POSITIONS.add(new double[]{0, 10, 0}); // 信号源 3
    }

    // 主方法
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); // 用于读取用户输入
        double p0 = -40; // 参考 RSSI 值（距离 1 米时的信号强度）
        double n = 2; // 信号传播路径损耗指数

        while (true) {
            System.out.println("请输入 3 个信号源的 RSSI 值（用逗号分隔，例如 -40,-50,-45）：");
            String input = scanner.nextLine(); // 读取用户输入
            if (input.equalsIgnoreCase("exit")) { // 如果用户输入 "exit"，退出程序
                break;
            }

            try {
                // 将用户输入的字符串拆分为 RSSI 值数组
                String[] rssiStrings = input.split(",");
                if (rssiStrings.length != SIGNAL_SOURCE_POSITIONS.size()) {
                    throw new IllegalArgumentException("必须输入 " + SIGNAL_SOURCE_POSITIONS.size() + " 个 RSSI 值。");
                }

                // 将 RSSI 值从字符串转换为 double 类型
                double[] rssiValues = new double[rssiStrings.length];
                for (int i = 0; i < rssiStrings.length; i++) {
                    rssiValues[i] = Double.parseDouble(rssiStrings[i].trim());
                }

                // 第一步：对 RSSI 值进行平滑处理
                double[] smoothedRSSI = smoothRSSI(rssiValues);

                // 第二步：根据平滑后的 RSSI 值计算与信号源的距离
                double[] distances = new double[smoothedRSSI.length];
                for (int i = 0; i < smoothedRSSI.length; i++) {
                    distances[i] = calculateDistance(smoothedRSSI[i], p0, n);
                }

                // 第三步：通过三边测量法计算目标的位置
                double[] targetPosition = trilateration(SIGNAL_SOURCE_POSITIONS, distances);

                // 输出计算得到的目标位置
                System.out.printf("目标位置计算结果：x=%.2f, y=%.2f, z=%.2f%n",
                        targetPosition[0], targetPosition[1], targetPosition[2]);
            } catch (Exception e) {
                // 捕获并显示错误信息
                System.out.println("错误：" + e.getMessage());
            }
        }

        scanner.close(); // 关闭扫描器
    }

    /**
     * 根据 RSSI 值计算与信号源的距离
     * @param rssi 接收到的 RSSI 值
     * @param p0 参考 RSSI 值（距离 1 米时的信号强度）
     * @param n 信号传播路径损耗指数
     * @return 计算得到的距离（单位：米）
     */
    public static double calculateDistance(double rssi, double p0, double n) {
        return Math.pow(10, (p0 - rssi) / (10 * n));
    }

    /**
     * 使用三边测量法计算目标位置
     * @param positions 信号源的位置列表
     * @param distances 与每个信号源的距离
     * @return 计算得到的目标位置 (x, y, z)
     */
    public static double[] trilateration(List<double[]> positions, double[] distances) {
        if (positions.size() < 3 || distances.length < 3) {
            throw new IllegalArgumentException("至少需要 3 个信号源才能进行三边测量。");
        }

        // 提取信号源的位置
        double x1 = positions.get(0)[0], y1 = positions.get(0)[1], z1 = positions.get(0)[2];
        double x2 = positions.get(1)[0], y2 = positions.get(1)[1], z2 = positions.get(1)[2];
        double x3 = positions.get(2)[0], y3 = positions.get(2)[1], z3 = positions.get(2)[2];

        // 提取与目标的距离
        double d1 = distances[0], d2 = distances[1], d3 = distances[2];

        // 在二维平面 (x, y) 上进行计算，假设 z 方向一致
        double A = 2 * (x2 - x1);
        double B = 2 * (y2 - y1);
        double C = d1 * d1 - d2 * d2 - x1 * x1 - y1 * y1 + x2 * x2 + y2 * y2;
        double D = 2 * (x3 - x2);
        double E = 2 * (y3 - y2);
        double F = d2 * d2 - d3 * d3 - x2 * x2 - y2 * y2 + x3 * x3 + y3 * y3;

        // 解算 x 和 y 的位置
        double x = (C * E - F * B) / (E * A - B * D);
        double y = (C * D - A * F) / (B * D - A * E);

        // 假设 z 为信号源 z 坐标的平均值
        double z = (z1 + z2 + z3) / 3;

        return new double[]{x, y, z};
    }

    /**
     * 使用简单的滑动平均法对 RSSI 值进行平滑处理
     * @param rssiValues 原始 RSSI 值数组
     * @return 平滑后的 RSSI 值数组
     */
    public static double[] smoothRSSI(double[] rssiValues) {
        double[] smoothedValues = new double[rssiValues.length];
        int windowSize = 3; // 滑动窗口大小

        for (int i = 0; i < rssiValues.length; i++) {
            double sum = 0;
            int count = 0;
            // 计算窗口内的平均值
            for (int j = i; j > i - windowSize && j >= 0; j--) {
                sum += rssiValues[j];
                count++;
            }
            smoothedValues[i] = sum / count;
        }

        return smoothedValues;
    }


}
