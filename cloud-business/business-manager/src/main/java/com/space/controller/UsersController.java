package com.space.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.space.core.entry.p.Pusers;
import com.space.core.params.SaveUsersParams;
import com.space.core.response.ApiResult;
import com.space.core.params.HomeParams;
import com.space.service.UsersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 张佳宁
 * @version 1.0.0
 * @description: TODO
 * @date 2024/3/26 14:43
 */
@RestController
@Api(tags = "用户信息API")
@RequestMapping("/user")
@Slf4j
public class UsersController {
    @Resource
    private UsersService usersService;

    @ApiOperation(value = "获取用户列表", notes = "Users")
    @PostMapping("/query")
    public ApiResult<Page<Pusers>> query(@RequestBody HomeParams params) {

    return ApiResult.newInstance(usersService.queryHomeList(params));
    }


    @ApiOperation(value = "获取用户列表", notes = "Users")
    @PostMapping("/save")
    public ApiResult  save(@RequestBody SaveUsersParams users) {

        return usersService.saveUser(users);
    }


}
