package com.space.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.space.core.entry.p.Pusers;
import com.space.core.params.HomeParams;
import com.space.core.params.SaveUsersParams;
import com.space.core.response.ApiResult;
import com.space.core.utils.SecureUtils;
import com.space.mapper.UsersMapper;
import com.space.service.UsersService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @author 张佳宁
 * @description:
 * @date: 2024/3/26 15:25
 */
@Service
@Slf4j
public class UsersServiceImpl extends ServiceImpl<UsersMapper, Pusers> implements UsersService {
    @Resource
    private UsersMapper usersMapper;


    /**
     * @description: 查询用户
     * @param: null
     * @return:
     * @author 张佳宁
     * @date: 2024/12/24 15:07
     */
    @Override
    public Page<Pusers> queryHomeList(HomeParams params) {
        QueryWrapper<Pusers> wrapper = new QueryWrapper<>();
        if (!ObjectUtil.isEmpty(params.getUserName())) {
            wrapper.eq("username", params.getUserName());
        }
        if (!ObjectUtil.isEmpty(params.getEmail())) {
            wrapper.eq("email", params.getEmail());
        }
        return usersMapper.selectPage(params.toPage(), wrapper);
    }

    /**
     * @description: 保存用户
     * @param: null
     * @return:
     * @author 张佳宁
     * @date: 2024/12/24 15:07
     */
    @Override
    public ApiResult saveUser(SaveUsersParams users) {

        Pusers pusers = new Pusers();
        pusers.setUserId(IdUtil.simpleUUID());
        pusers.setUsername(users.getUserName());
        pusers.setEmail(users.getEmail());
        pusers.setHeadUrl(users.getHeadUrl());
        pusers.setStatusMessage(users.getStatusMessage());
        //代码优化点： 后期slat加在db中，params中的密码做明文加密
        final String pwd = users.getPassword() + "jnlovecx202405181021";
        final String passwordMD5 = SecureUtils.encode(pwd);
        pusers.setPasswordHash(passwordMD5);
        usersMapper.insert(pusers);
        return ApiResult.newInstance();
    }
}
