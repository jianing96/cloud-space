package com.space.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.space.core.entry.p.Pmessage;
import com.space.core.entry.p.Pusers;
import com.space.core.params.HomeParams;
import com.space.core.params.SaveUsersParams;
import com.space.core.response.ApiResult;
import com.space.core.utils.SecureUtils;
import com.space.mapper.MessageMapper;
import com.space.mapper.UsersMapper;
import com.space.service.MessageService;
import com.space.service.UsersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * @author 张佳宁
 * @description:
 * @date: 2024/3/26 15:25
 */
@Service
@Slf4j
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Pmessage> implements MessageService {
    @Resource
    private MessageMapper messageMapper;


}
