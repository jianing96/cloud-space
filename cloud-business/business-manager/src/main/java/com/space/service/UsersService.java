package com.space.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.space.core.entry.p.Pusers;
import com.space.core.params.HomeParams;
import com.space.core.params.SaveUsersParams;
import com.space.core.response.ApiResult;

/**
 * @description:
 * @author 张佳宁
 * @date: 2024/3/26 15:25
 */
public interface UsersService extends IService<Pusers> {


    Page<Pusers> queryHomeList(HomeParams params);

    ApiResult saveUser(SaveUsersParams users);
}
