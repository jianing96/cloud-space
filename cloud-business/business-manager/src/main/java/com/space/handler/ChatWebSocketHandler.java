package com.space.handler;

import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

/**
 * @author 张佳宁
 * @version 1.0.0
 * @description: TODO
 * @date 2025/1/8 10:24
 */
public class ChatWebSocketHandler  extends TextWebSocketHandler {
    @Override
    public void handleTextMessage(WebSocketSession session, org.springframework.web.socket.TextMessage message) {
        // 处理消息发送和接收
        // 可以根据用户ID来分发消息给特定的用户
        System.out.println("Message received: " + message.getPayload());
    }
}
