package com.space.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.space.core.entry.p.Pmessage;
import com.space.core.entry.p.Pusers;

/**
 * @author 张佳宁
 * @version 1.0.0
 * @description: TODO
 * @date 2024/3/26 15:24
 */
public interface MessageMapper extends BaseMapper<Pmessage> {
}
