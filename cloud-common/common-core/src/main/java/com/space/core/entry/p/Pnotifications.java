package com.space.core.entry.p;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.space.core.utils.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author 张佳宁
 * @description:
 * @param: null
 * @return:
 * @date: 2023/3/8 3:06 PM
 */
@Data
@Accessors(chain = true)
@TableName("p_notifications")
public class Pnotifications {
    @TableId(value = "id", type = IdType.AUTO)
    @JsonSerialize(using = ToStringSerializer.class)
    protected Long notification_id;
    @TableField("userId")
    private String user_id;

    @TableField("type")
    private String type;

    @TableField("content")
    private String content;

    @TableField("is_read")
    private Integer isRead;

    @TableField("created_at")
    private Date createdAt;
}
