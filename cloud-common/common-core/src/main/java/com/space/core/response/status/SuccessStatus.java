package com.space.core.response.status;


public enum SuccessStatus {
    /*成功返回*/
    OPERATION_SUCCESS("20000", "请求成功");

    private String status;

    private String message;

    private SuccessStatus(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String status() {
        return status;
    }

    public String message() {
        return message;
    }
}
