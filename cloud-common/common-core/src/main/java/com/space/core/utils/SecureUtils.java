package com.space.core.utils;


import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.SecureUtil;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;


/**
 * @author 张佳宁
 * @description:登陆密码工具类
 * @date: 2024/3/25 14:17
 */
public class SecureUtils {


    private static final String hexDigits[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};


    private static final String key = "hnlh-pactera";

    private static final cn.hutool.crypto.symmetric.DES DES = SecureUtil.des(key.getBytes());


    public static String passwordEncode(String str) {
        return DES.encryptHex(str);
    }

    public static String passwordDecryptStr(String str) {
        return DES.decryptStr(str);
    }


    private static String byteArrayToHexString(byte b[]) {
        StringBuffer resultSb = new StringBuffer();
        for (int i = 0; i < b.length; i++) {
            resultSb.append(byteToHexString(b[i]));
        }

        return resultSb.toString();
    }


    private static String byteToHexString(byte b) {
        int n = b;
        if (n < 0) {
            n += 256;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return hexDigits[d1] + hexDigits[d2];
    }


    /**
     * md5加密
     *
     * @param origin 加密串
     * @return:
     * @Author: LL
     * @Date: 2020/7/24  10:53
     */
    public static String encode(String origin) {
        return encode(origin, "UTF-8");
    }

    public static String encode(String origin, String charsetname) {
        String resultString = null;
        try {
            resultString = new String(origin);
            MessageDigest md = MessageDigest.getInstance("MD5");
            if (charsetname == null || "".equals(charsetname)) {
                resultString = byteArrayToHexString(md.digest(resultString.getBytes()));
            } else {
                resultString = byteArrayToHexString(md.digest(resultString.getBytes(charsetname)));
            }
        } catch (Exception exception) {
        }
        return resultString;
    }

    public static String decrypt(String src, String key) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decode = Base64.decode(src);
        byte[] original = cipher.doFinal(decode);
        String originalString = new String(original);
        return originalString;
    }

}
