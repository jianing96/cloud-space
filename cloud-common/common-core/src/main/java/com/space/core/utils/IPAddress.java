package com.space.core.utils;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;


@Slf4j
public class IPAddress {
    private final static String UNKNOWN_STR = "unknown";

    /*
     * 在一般情况下使用Request.getRemoteAddr()即可，但是经过nginx等反向代理软件后，这个方法会失效。
     * 本方法先从Header中获取X-Real-IP，如果不存在再从X-Forwarded-For获得第一个IP(用,分割)，
     * 如果还不存在则调用Request .getRemoteAddr()。
     **/
    public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
        ip = request.getHeader("X-Real-IP");
        if (isEmptyIP(ip)) {
            ip = request.getHeader("X-Forwarded-For");
            if (isEmptyIP(ip)) {
                ip = request.getHeader("Proxy-Client-IP");
                if (isEmptyIP(ip)) {
                    ip = request.getHeader("WL-Proxy-Client-IP");
                    if (isEmptyIP(ip)) {
                        ip = request.getHeader("HTTP_CLIENT_IP");
                        if (isEmptyIP(ip)) {
                            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
                            if (isEmptyIP(ip)) {
                                ip = request.getRemoteAddr();
                                if ("127.0.0.1".equals(ip) || "0:0:0:0:0:0:0:1".equals(ip)) {
                                    // 根据网卡取本机配置的IP
                                    ip = getLocalAddr();
                                }
                            }
                        }
                    }
                }
            }
        } else if (ip.length() > 15) {
            String[] ips = ip.split(",");
            for (int index = 0; index < ips.length; index++) {
                String strIp = ips[index];
                if (!isEmptyIP(ip)) {
                    ip = strIp;
                    break;
                }
            }
        }
        return ip;
    }

    public static String getServerIp() {
        try {
            InetAddress addr = InetAddress.getLocalHost();
            String ip = addr.getHostAddress();
            return ip;
        } catch (UnknownHostException e) {

        }
        return null;
    }

    private static boolean isEmptyIP(String ip) {
        if (StrUtil.isEmpty(ip) || UNKNOWN_STR.equalsIgnoreCase(ip)) {
            return true;
        }
        return false;
    }

    /**
     * 获取本机的IP地址
     */
    public static String getLocalAddr() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.info("InetAddress.getLocalHost()-error", e);
        }
        return "";
    }
}
