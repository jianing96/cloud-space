package com.space.core.utils;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;



@Data
public class BaseQuery implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long listLimit;

    private Map<String, Object> params;

    public Map<String, Object> getParams() {
        if (params == null) {
            params = new HashMap<>();
        }
        return params;
    }

}
