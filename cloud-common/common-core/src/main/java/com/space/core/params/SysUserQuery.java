package com.space.core.params;

import com.space.core.utils.BasePageQuery;
import lombok.Data;

/**
 * @author 张佳宁
 * @version 1.0.0
 * @description: TODO
 * @date 2024/3/12 4:42 PM
 */
@Data
public class SysUserQuery extends BasePageQuery {

    private String phone;
    private String userName;
    private String passWord;



}
