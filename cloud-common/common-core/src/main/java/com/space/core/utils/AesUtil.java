package com.space.core.utils;


import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by l05826 on 2020/5/15.
 */
public class AesUtil {
    private static final Logger logger = LoggerFactory.getLogger(AesUtil.class);


    private static final String PASSWORD = "Hnlh@kaoq2023323"; // 加密秘钥

    /**
     * 加密
     *
     * @param sSrc 需要加密的字符串
     * @param sKey 此处使用AES-128-ECB加密模式，key需要为16位。
     * @return
     * @throws Exception
     */
    public static String Encrypt(String sSrc, String sKey) throws Exception {

        // 判断Key是否为16位
        if (sKey.length() != 16) {
            logger.info("密钥长度校验失败");
            return null;
        }
        byte[] raw = sKey.getBytes("utf-8");
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");//"算法/模式/补码方式"
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(sSrc.getBytes("utf-8"));

        return new Base64().encodeToString(encrypted);//此处使用BASE64做转码功能，同时能起到2次加密的作用。
    }

    /**
     * 解密
     *
     * @param sSrc 需要解密的字符串
     * @param sKey 此处使用AES-128-ECB加密模式，key需要为16位。
     * @return
     * @throws Exception
     */
    public static String Decrypt(String sSrc, String sKey) throws Exception {
        try {
            // 判断Key是否正确
            if (sKey == null) {
                sKey = PASSWORD;
            }
            // 判断Key是否为16位
            if (sKey.length() != 16) {
                logger.info("密钥长度校验失败");
                return null;
            }
            byte[] raw = sKey.getBytes("utf-8");
            SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec);
            byte[] encrypted1 = new Base64().decode(sSrc);//先用base64解密
            try {
                byte[] original = cipher.doFinal(encrypted1);
                String originalString = new String(original, "utf-8");
                return originalString;
            } catch (Exception e) {
                System.out.println(e.toString());
                return null;
            }
        } catch (Exception ex) {
            System.out.println(ex.toString());
            return null;
        }
    }


    /**
     * 获取加密的身份证号
     *
     * @param idcard 身份证号
     * @return java.lang.String
     * @author mengwei
     * @date 2021/8/10 10:14
     */
    public static String encryptByIdcard(String idcard) {
        return idcard.replaceAll("(\\w{4})\\w*(\\w{4})", "$1******$2");
    }

    /**
     * 获取加密的手机号
     *
     * @param phone 手机号
     * @return java.lang.String
     * @author mengwei
     * @date 2021/8/10 10:14
     */
    public static String encryptByPhone(String phone) {
        return phone.replaceAll("(\\w{3})\\w*(\\w{4})", "$1****$2");
    }


    public static void main(String[] args) throws Exception {
        /*
         * 此处使用AES-128-ECB加密模式，key需要为16位。
         */
        String cKey = "Hnlh@kaoq2023323";
        // 需要加密的字串
        String cSrc = "zhangjn";
        System.out.println(cSrc);
        // 加密
        String enString = Encrypt(cSrc, cKey);
        System.out.println("加密后的字串是：" + enString);

        // 解密
        String DeString = Decrypt(enString, cKey);
        System.out.println("解密后的字串是：" + DeString);
    }
}
