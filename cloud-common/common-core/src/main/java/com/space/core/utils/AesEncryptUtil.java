package com.space.core.utils;

import cn.hutool.core.codec.Base64Decoder;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.symmetric.AES;

/**
 * @author MengEn.Cao
 * @date 2023-01-04 11:17
 **/
public class AesEncryptUtil {



    public static final String BASE64_KEY = "4LfCkniQVTD4UFKSxd1jzQ==";

    public static String encrypt(String obj) {
        if(StrUtil.isEmpty(obj)){
            return null ;
        }
        byte[] key = Base64Decoder.decode(BASE64_KEY);
        AES aes = SecureUtil.aes(key);
        // HEX加密
        return aes.encryptHex(obj);
    }

    public static String decrypt(String obj) {
        if(StrUtil.isEmpty(obj)){
            return null ;
        }
        byte[] key = Base64Decoder.decode(BASE64_KEY);
        AES aes = SecureUtil.aes(key);
        // HEX解密
        return aes.decryptStr(obj);
    }


    public static void main(String[] args) {
        //将base64的密钥转换为bytes
        byte[] key = Base64Decoder.decode(BASE64_KEY);
        //创建AES
        AES aes = SecureUtil.aes(key);
        //加密
//        String encrypt = encrypt("620104198604120031");
        String encrypt = encrypt("zhangap.hnlh@sinopec.com");
        //解密
        String decryptStr = aes.decryptStr(encrypt);
        //620104198604120031
        System.out.println("解密后: "+decryptStr);

    }

}
