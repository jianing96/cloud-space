package com.space.core.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.space.core.entry.sys.SysUser;
import com.space.core.params.SysUserQuery;
import com.space.core.response.ApiResult;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author bzg
 * @since 2023-03-14
 */
public interface SysUserService extends IService<SysUser> {

    ApiResult queryUsersByParams(SysUserQuery query);
}
