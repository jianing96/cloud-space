package com.space.core.entry.p;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author 张佳宁
 * @description:
 * @param: null
 * @return:
 * @date: 2023/3/8 3:06 PM
 */
@Data
@Accessors(chain = true)
@TableName("p_friendships")
public class Pfriendships {
    @TableField("friendship_id")
    protected String friendshipId;
    @TableField("user_id")
    private String userId;
    @TableField("friend_id")
    private String friendId;
    @TableField("status")
    private String status;
    @TableField("created_at")
    private Date createdAt;
}
