package com.space.core.entry.p;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author 张佳宁
 * @description:
 * @param: null
 * @return:
 * @date: 2023/3/8 3:06 PM
 */
@Data
@Accessors(chain = true)
@TableName("p_group_members")
public class PgroupMembers {
    @TableField("group_member_id")
    protected String groupMemberId;
    @TableField("group_id")
    private String groupId;
    @TableField("user_id")
    private String userId;
    @TableField("role")
    private String role;
    @TableField("joined_at")
    private Date joinedAt;
}
