package com.space.core.entry.p;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author 张佳宁
 * @description:
 * @param: null
 * @return:
 * @date: 2023/3/8 3:06 PM
 */
@Data
@Accessors(chain = true)
@TableName("p_group_messages")
public class PgroupMessages {
    @TableField("message_id")
    protected String messageId;
    @TableField("group_id")
    private String groupId;
    @TableField("sender_id")
    private String senderId;
    @TableField("content")
    private String content;
    @TableField("message_type")
    private String messageType;
    @TableField("sent_at")
    private Date sentAt;
}
