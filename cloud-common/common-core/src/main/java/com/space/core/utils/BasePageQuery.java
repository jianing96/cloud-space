package com.space.core.utils;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import java.util.Map;


@Data
public class BasePageQuery extends BaseQuery {
    private static final long serialVersionUID = 1L;

    private Long pageSize = 10L;

    private Long pageNum = 1L;

    @TableField(exist = false)
    private Map<String,Object> params;

    public Page toPage() {
        //mybatis-plus分页
        return new Page(this.pageNum, this.pageSize);
    }
}
