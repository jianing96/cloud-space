package com.space.core.params;

import com.space.core.utils.BasePageQuery;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author 张佳宁
 * @version 1.0.0
 * @description: TODO
 * @date 2024/3/26 14:45
 */
@Data
public class HomeParams extends BasePageQuery {

    private String userName;
    private String email;

}
