package com.space.core.response;


import com.space.core.response.status.ErrorStatus;
/** 
 * @description: 封装状态返回数据结果集 
 * @author 张佳宁
 * @date: 2024/3/12 3:42 PM
 */ 
public final class ApiResult<Data> {


    // 成功状态码
    public static final String RESULT_OK_CODE = "200";
    // 失败状态码
    public static final String RESULT_FAIL_CODE = "400";
    // 成功消息
    public static final String RESULT_OK_MSG = "success";
    // 成功消息
    public static final String RESULT_FAIL_MSG = "fail";

    // 状态码
    private final String status;
    // 错误消息
    private final String message;
    // 数据正文
    private final Data data;
    // 时间戳
    private final Long time = System.currentTimeMillis();

    public ApiResult(String status, String message, Data data) {
        this.status = status;
        this.message = message;
        this.data = data;
    }


    public static <Data> ApiResult<Data> newInstance(Data data) {

        return new ApiResult<>(RESULT_OK_CODE, RESULT_OK_MSG, data);
    }


    public static <Data> ApiResult<Data> newInstance(String code, String message, Data data) {
        return new ApiResult<>(code, message, data);
    }

    public static ApiResult newInstance() {
        return new ApiResult(RESULT_OK_CODE, RESULT_OK_MSG, null);
    }

    public static ApiResult newInstanceStr(String result) {
        return new ApiResult(RESULT_OK_CODE, RESULT_OK_MSG + result, null);
    }


    public static ApiResult fail(String message) {
        return new ApiResult(RESULT_FAIL_CODE, message, null);
    }

    public static ApiResult fail() {
        return new ApiResult(RESULT_FAIL_CODE, RESULT_FAIL_MSG, null);
    }

    public static ApiResult fail(ErrorStatus status) {
        return new ApiResult(status.status(), status.message(), null);
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public Long getTime() {
        return time;
    }
}
