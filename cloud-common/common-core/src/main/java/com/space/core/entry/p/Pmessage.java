package com.space.core.entry.p;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author 张佳宁
 * @description:
 * @param: null
 * @return:
 * @date: 2023/3/8 3:06 PM
 */
@Data
@Accessors(chain = true)
@TableName("p_messages")
public class Pmessage {
    @TableId(value = "message_id", type = IdType.AUTO)
    @JsonSerialize(using = ToStringSerializer.class)
    protected Long messageId;
    @TableField("sender_id")
    private String senderId;

    @TableField("receiver_id")
    private String receiverId;

    @TableField("content")
    private String content;

    @TableField("message_type")
    private String messageType;

    @TableField("is_read")
    private Integer isRead;

    @TableField("sent_at")
    private Date sentAt;
}
