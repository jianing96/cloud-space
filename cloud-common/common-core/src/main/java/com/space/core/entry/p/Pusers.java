package com.space.core.entry.p;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.space.core.utils.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author 张佳宁
 * @description:
 * @param: null
 * @return:
 * @date: 2023/3/8 3:06 PM
 */
@Data
@Accessors(chain = true)
@TableName("p_users")
public class Pusers{

    @TableField("user_id")
    private String userId;

    @TableField("username")
    private String username;

    @TableField("email")
    private String email;

    @TableField("password_hash")
    private String passwordHash;

    @TableField("head_url")
    private String headUrl;

    @TableField("status_message")
    private String statusMessage;
    @TableField("created_at")
    private Date createdAt;


}
