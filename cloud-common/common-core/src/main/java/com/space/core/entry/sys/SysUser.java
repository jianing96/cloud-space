package com.space.core.entry.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.space.core.utils.BaseEntity;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author 张佳宁
 * @description:
 * @param: null
 * @return:
 * @date: 2023/3/8 3:06 PM
 */
@Data
@Accessors(chain = true)
@TableName("sys_user")
public class SysUser extends BaseEntity<Long> {
    @TableField("dept_id")
    private Long deptId;

    @TableField("user_name")
    private String userName;

    @TableField("nick_name")
    private String nickName;

    @TableField("user_type")
    private Integer userType;

    @TableField("email")
    private String email;

    @TableField("password")
    private String password;
    @TableField("phonenumber")
    private String phonenumber;

    @TableField("avatar")
    private String avatar;
    @TableField("status")
    private String status;
    @TableField("del_flag")
    private String delFlag;
    @TableField("login_ip")
    private String loginIp;
    @TableField("login_date")
    private String loginDate;
    /**
     * token
     */
    @TableField(exist = false)
    private String token;


}
