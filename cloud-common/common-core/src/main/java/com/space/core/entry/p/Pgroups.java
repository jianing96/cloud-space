package com.space.core.entry.p;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @author 张佳宁
 * @description:
 * @param: null
 * @return:
 * @date: 2023/3/8 3:06 PM
 */
@Data
@Accessors(chain = true)
@TableName("p_groups")
public class Pgroups {
    @TableField("group_id")
    protected String groupId;
    @TableField("group_name")
    private String groupName;

    @TableField("created_by")
    private String createdBy;
    @TableField("created_at")
    private Date createdAt;
}
