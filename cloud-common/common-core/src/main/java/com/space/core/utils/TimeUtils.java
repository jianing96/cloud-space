package com.space.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 时间相关操作工具类
 *
 * @author：Peill
 * @since：2017年8月21日 下午2:00:22
 */
public class TimeUtils {

    // 目录
    // millis2String:将时间戳转为时间字符串
    // string2Millis:将时间字符串转为时间戳
    // string2Date:将时间字符串转为Date类型
    // date2String:将Date类型转为时间字符串
    // date2Millis:将Date类型转为时间戳
    // millis2Date:将时间戳转为Date类型
    // dateReckon:日期+-天数
    // yearReckon:日期+-年数
    // getNowTimeMills:获取当前毫秒时间戳
    // getNowTimeString:获取当前时间字符串
    // getNowTimeDate:获取当前Date getWeek,
    // getNowTimeDateString:获取当前Date字符串
    // getWeekIndex:获取星期
    // getWeekOfMonth:获取月份中的第几周
    // getWeekOfYear:获取年份中的第几周
    // parseMillisecone:时间差计算 return string 0天0时11分55秒
    // getDifference:时间差计算return int
    // getChineseZodiac:获取生肖
    // getZodiac:获取星座
    // getNearlyWeekDates:获取最近一周的所有日期
    // getNearlyMonthDates:获取最近一月的所有日期
    // getNearlyYearDates:获取最近一年的所有月份
    // getBetweenDates:补全给定起止时间区间内的所有日期
    // getWeeksBetweenDates:补全给定起止时间区间内的所有周
    // getMonthsBetweenDates:补全给定起止时间区间内的所有月
    // 注意：SimpleDateFormat不是线程安全的，线程安全需用{@code ThreadLocal<SimpleDateFormat>}

    static final SimpleDateFormat DEFAULT_SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());

    static final SimpleDateFormat DEFAULT_SIMPLE_SDF = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    public static final String DEFAULT_TIME = "yyyy-MM-dd HH:mm:ss";

    public static final String DEFAULT_SIMPLE_TIME = "yyyy-MM-dd";
    public static final String DEFAULT_SIMPLE_TIME1 = "yyyy-MM-ddHH:mm";
    public static final String DEFAULT_SIMPLE_TIME2 = "yyyy-MM-dd HH:mm";
    public static final String DEFAULT_SIMPLE_TIME3 = "yyyy/MM/dd";
    public static final String DEFAULT_SIMPLE_TIME4 = "yyyyMMddHHmmss";
    public static final String DEFAULT_SIMPLE_TIME5 = "yyyyMMdd";
    public static final String DEFAULT_SIMPLE_TIME6 = "yyyyMM";
    public static final String DEFAULT_SIMPLE_TIME7 = "HH:mm";
    public static final String DEFAULT_SIMPLE_TIME8 = "dd";
    public static final String SUFFIX = "00:00:00";

    public static final String DEFAULT_SIMPLE_MONTH = "yyyy-MM";

    public static final String DEFAULT_CHINA_GMT = "EEE MMM dd yyyy HH:mm:ss 'GMT'Z";

    public static final long DAY = 1000 * 60 * 60 * 24;

    public static final long HOUR = 1000 * 60 * 60;

    public static final long MINUTE = 1000 * 60;

    public static final long SECOND = 1000;

    /**
     * 将时间戳转为时间字符串 格式为yyyy-MM-dd HH:mm:ss
     *
     * @param millis 毫秒时间戳
     * @return 时间字符串
     */
    public static String millis2String(long millis) {
        return DEFAULT_SDF.format(new Date(millis));
    }

    /**
     * 将时间戳转为时间字符串 格式为pattern
     *
     * @param millis  毫秒时间戳
     * @param pattern 时间格式
     * @return 时间字符串
     */
    public static String millis2String(long millis, String pattern) {
        return new SimpleDateFormat(pattern, Locale.getDefault()).format(new Date(millis));
    }

    /**
     * 将时间字符串转为时间戳 time格式为yyyy-MM-dd HH:mm:ss
     *
     * @param time 时间字符串
     * @return 毫秒时间戳
     */
    public static long string2Millis(String time) {
        return string2Millis(time, DEFAULT_TIME);
    }

    /**
     * 将时间字符串转为时间戳 time格式为pattern
     *
     * @param time    时间字符串
     * @param pattern 时间格式
     * @return 毫秒时间戳
     */
    public static long string2Millis(String time, String pattern) {
        try {
            return new SimpleDateFormat(pattern, Locale.getDefault()).parse(time).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 将时间字符串转为Date类型 time格式为yyyy-MM-dd
     *
     * @param time 时间字符串
     * @return Date类型
     */
    public static Date string2DateDay(String time) {
        return string2Date(time, DEFAULT_SIMPLE_TIME);
    }

    /**
     * 将时间字符串转为Date类型 time格式为yyyy-MM-dd HH:mm:ss
     *
     * @param time 时间字符串
     * @return Date类型
     */
    public static Date string2Date(String time) {
        return string2Date(time, DEFAULT_SIMPLE_TIME);
    }

    /**
     * 将时间字符串转为Date类型
     * <p>
     * time格式为pattern
     *
     * @param time    时间字符串
     * @param pattern 时间格式
     * @return Date类型
     */
    public static Date string2Date(String time, String pattern) {
        return new Date(string2Millis(time, pattern));
    }

    /**
     * 将Date类型转为时间字符串 格式为yyyy-MM-dd HH:mm:ss
     *
     * @param date Date类型时间
     * @return 时间字符串
     */
    public static String date2String(Date date) {
        return date2String(date, DEFAULT_TIME);
    }

    /**
     * 将Date类型转为时间字符串 格式为yyyy-MM-dd
     *
     * @param date Date类型时间
     * @return 时间字符串
     */
    public static String date3String(Date date) {
        return date2String(date, DEFAULT_SIMPLE_TIME);
    }

    /**
     * 将Date类型转为时间字符串 格式为pattern
     *
     * @param date    Date类型时间
     * @param pattern 时间格式
     * @return 时间字符串
     */
    public static String date2String(Date date, String pattern) {
        return new SimpleDateFormat(pattern, Locale.getDefault()).format(date);
    }

    /**
     * 将Date类型转为时间字符串 格式为pattern
     *
     * @param date    Date类型时间
     * @param pattern 时间格式
     * @return 时间字符串
     */
    public static String date2StringToEnglisg(Date date, String pattern) {
        return new SimpleDateFormat(pattern, Locale.ENGLISH).format(date);
    }

    /**
     * 将Date类型转为时间戳
     *
     * @param date Date类型时间
     * @return 毫秒时间戳
     */
    public static long date2Millis(Date date) {
        return date.getTime();
    }

    /**
     * 将时间戳转为Date类型
     *
     * @param millis 毫秒时间戳
     * @return Date类型时间
     */
    public static Date millis2Date(long millis) {
        return new Date(millis);
    }

    /**
     * 日期+-天数
     *
     * @param date
     * @param num  增加的天数，整数为+，负数-
     * @return Date
     */
    public static Date dateReckon(Date date, Integer num) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, num);// 把日期往后增加一天.整数往后推,负数往前移动
        date = calendar.getTime(); // 这个时间就是日期往后推一天的结果
        return date;
    }

    /**
     * 日期+-小时
     *
     * @param date
     * @param num  增加的天数，整数为+，负数-
     * @return Date
     */
    public static Date hourReckon(Date date, Integer num) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, num);// 把日期往后增加一天.整数往后推,负数往前移动
        date = calendar.getTime(); // 这个时间就是日期往后推一天的结果
        return date;
    }

    /**
     * 日期+-分钟
     *
     * @param date
     * @param num  增加的分钟数，正数为+，负数-
     * @return Date
     */
    public static Date minuteReckon(Date date, Integer num) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, num);// 把日期往后增加分钟数.整数往后推,负数往前移动
        date = calendar.getTime(); // 这个时间就是日期往后增加分钟数的结果
        return date;
    }

    /**
     * 日期+-秒
     *
     * @param date
     * @param num  增加的秒数，正数为+，负数-
     * @return Date
     */
    public static Date secondReckon(Date date, Integer num) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.SECOND, num);// 把日期往后增加秒数.整数往后推,负数往前移动
        date = calendar.getTime(); // 这个时间就是日期往后增加秒数的结果
        return date;
    }

    /**
     * 日期+-年数
     *
     * @param date
     * @param num  增加的年数，整数为+，负数-
     * @return Date
     */
    public static Date yearReckon(Date date, Integer num) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, num);// 把日期往后增加一年.整数往后推,负数往前移动
        date = calendar.getTime(); // 这个时间就是日期往后推一年的结果
        return date;
    }

    /**
     * 日期+-月份
     *
     * @param date
     * @param num  增加的月份，整数为+，负数-
     * @return Date
     */
    public static Date monthReckon(Date date, Integer num) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, num);// 把日期往后增加一月.整数往后推,负数往前移动
        date = calendar.getTime(); // 这个时间就是日期往后推一月的结果
        return date;
    }

    /**
     * 日期+-天数
     *
     * @param millis
     * @param num    增加的天数，整数为+，负数-
     * @return Date
     */
    public static Date dateReckon(long millis, Integer num) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(millis2Date(millis));
        calendar.add(Calendar.DATE, num);// 把日期往后增加一天.整数往后推,负数往前移动
        Date date = calendar.getTime(); // 这个时间就是日期往后推一天的结果
        return date;
    }

    /**
     * 日期+-天数
     *
     * @param time 时间字符串
     * @param num  增加的天数，整数为+，负数-
     * @return Date
     */
    public static Date dateReckon(String time, Integer num) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(string2Date(time));
        calendar.add(Calendar.DATE, num);// 把日期往后增加一天.整数往后推,负数往前移动
        Date date = calendar.getTime(); // 这个时间就是日期往后推一天的结果
        return date;
    }

    /**
     * 日期+-天数
     *
     * @param time 时间字符串
     * @param num  增加的天数，整数为+，负数-
     * @return Date
     */
    public static Date dateReckon(String time, Integer num, String pattern) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(string2Date(time, pattern));
        calendar.add(Calendar.DATE, num);// 把日期往后增加一天.整数往后推,负数往前移动
        Date date = calendar.getTime(); // 这个时间就是日期往后推一天的结果
        return date;
    }

    /**
     * 获取当前毫秒时间戳
     *
     * @return 毫秒时间戳
     */
    public static long getNowTimeMills() {
        return System.currentTimeMillis();
    }

    /**
     * 获取当前时间字符串 格式为yyyy-MM-dd HH:mm:ss
     *
     * @return 时间字符串
     */
    public static String getNowTimeString() {
        return millis2String(System.currentTimeMillis(), DEFAULT_TIME);
    }

    /**
     * 获取当前Date
     *
     * @return Date类型时间
     */
    public static Date getNowTimeDate() {
        return new Date();
    }

    /**
     * 获取当前Date字符串
     *
     * @return Date类型时间
     */
    public static String getNowTimeDateString() {
        return millis2String(System.currentTimeMillis(), DEFAULT_SIMPLE_TIME);
    }

    /**
     * 获取星期 time格式为yyyy-MM-dd HH:mm:ss
     *
     * @param time 时间字符串
     * @return 星期
     */
    public static String getWeek(String time) {
        return getWeek(string2Date(time, DEFAULT_TIME));
    }

    /**
     * 获取星期 time格式为pattern
     *
     * @param time    时间字符串
     * @param pattern 时间格式
     * @return 星期
     */
    public static String getWeek(String time, String pattern) {
        return getWeek(string2Date(time, pattern));
    }

    /**
     * 获取星期
     *
     * @param date Date类型时间
     * @return 星期
     */
    public static String getWeek(Date date) {
        return new SimpleDateFormat("EEEE", Locale.getDefault()).format(date);
    }

    /**
     * 获取星期
     *
     * @param millis 毫秒时间戳
     * @return 星期
     */
    public static String getWeek(long millis) {
        return getWeek(new Date(millis));
    }

    /**
     * 获取星期 注意：周日的Index才是1，周六为7 time格式为yyyy-MM-dd HH:mm:ss
     *
     * @param time 时间字符串
     * @return 1...5
     */
    public static int getWeekIndex(String time) {
        return getWeekIndex(string2Date(time, DEFAULT_TIME));
    }

    /**
     * 获取星期 注意：周日的Index才是1，周六为7 time格式为pattern
     *
     * @param time    时间字符串
     * @param pattern 时间格式
     * @return 1...7
     */
    public static int getWeekIndex(String time, String pattern) {
        return getWeekIndex(string2Date(time, pattern));
    }

    /**
     * 获取星期 注意：周日的Index才是1，周六为7
     *
     * @param date Date类型时间
     * @return 1...7
     */
    public static int getWeekIndex(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * 获取星期 注意：周日的Index才是1，周六为7
     *
     * @param millis 毫秒时间戳
     * @return 1...7
     */
    public static int getWeekIndex(long millis) {
        return getWeekIndex(millis2Date(millis));
    }

    /**
     * 获取月份中的第几周
     * <p>
     * 注意：国外周日才是新的一周的开始
     * </p>
     * <p>
     * time格式为yyyy-MM-dd HH:mm:ss
     * </p>
     *
     * @param time 时间字符串
     * @return 1...5
     */
    public static int getWeekOfMonth(String time) {
        return getWeekOfMonth(string2Date(time, DEFAULT_TIME));
    }

    /**
     * 获取月份中的第几周 注意：国外周日才是新的一周的开始 time格式为pattern
     *
     * @param time    时间字符串
     * @param pattern 时间格式
     * @return 1...5
     */
    public static int getWeekOfMonth(String time, String pattern) {
        return getWeekOfMonth(string2Date(time, pattern));
    }

    /**
     * 获取月份中的第几周 注意：国外周日才是新的一周的开始
     *
     * @param date Date类型时间
     * @return 1...5
     */
    public static int getWeekOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.WEEK_OF_MONTH);
    }

    /**
     * 获取月份中的第几周 注意：国外周日才是新的一周的开始
     *
     * @param millis 毫秒时间戳
     * @return 1...5
     */
    public static int getWeekOfMonth(long millis) {
        return getWeekOfMonth(millis2Date(millis));
    }

    /**
     * 获取年份中的第几周 注意：国外周日才是新的一周的开始 time格式为yyyy-MM-dd HH:mm:ss
     *
     * @param time 时间字符串
     * @return 1...54
     */
    public static int getWeekOfYear(String time) {
        return getWeekOfYear(string2Date(time, DEFAULT_TIME));
    }

    /**
     * 获取年份中的第几周 注意：国外周日才是新的一周的开始 time格式为pattern
     *
     * @param time    时间字符串
     * @param pattern 时间格式
     * @return 1...54
     */
    public static int getWeekOfYear(String time, String pattern) {
        return getWeekOfYear(string2Date(time, pattern));
    }

    /**
     * 获取年份中的第几周 注意：国外周日才是新的一周的开始
     *
     * @param date Date类型时间
     * @return 1...54
     */
    public static int getWeekOfYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * 获取年份中的第几周 注意：国外周日才是新的一周的开始
     *
     * @param millis 毫秒时间戳
     * @return 1...54
     */
    public static int getWeekOfYear(long millis) {
        return getWeekOfYear(millis2Date(millis));
    }

    private static final String[] CHINESE_ZODIAC = {"猴", "鸡", "狗", "猪", "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊"};

    /**
     * 获取生肖 time格式为yyyy-MM-dd HH:mm:ss
     *
     * @param time 时间字符串
     * @return 生肖
     */
    public static String getChineseZodiac(String time) {
        return getChineseZodiac(string2Date(time, DEFAULT_TIME));
    }

    /**
     * 获取生肖 time格式为pattern
     *
     * @param time    时间字符串
     * @param pattern 时间格式
     * @return 生肖
     */
    public static String getChineseZodiac(String time, String pattern) {
        return getChineseZodiac(string2Date(time, pattern));
    }

    /**
     * 获取生肖
     *
     * @param date Date类型时间
     * @return 生肖
     */
    public static String getChineseZodiac(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return CHINESE_ZODIAC[cal.get(Calendar.YEAR) % 12];
    }

    /**
     * 获取生肖
     *
     * @param millis 毫秒时间戳
     * @return 生肖
     */
    public static String getChineseZodiac(long millis) {
        return getChineseZodiac(millis2Date(millis));
    }

    /**
     * 获取生肖
     *
     * @param year 年
     * @return 生肖
     */
    public static String getChineseZodiac(int year) {
        return CHINESE_ZODIAC[year % 12];
    }

    private static final String[] ZODIAC = {"水瓶座", "双鱼座", "白羊座", "金牛座", "双子座", "巨蟹座", "狮子座", "处女座", "天秤座", "天蝎座", "射手座", "魔羯座"};
    private static final int[] ZODIAC_FLAGS = {20, 19, 21, 21, 21, 22, 23, 23, 23, 24, 23, 22};

    /**
     * 获取星座 time格式为yyyy-MM-dd HH:mm:ss
     *
     * @param time 时间字符串
     * @return 生肖
     */
    public static String getZodiac(String time) {
        return getZodiac(string2Date(time, DEFAULT_TIME));
    }

    /**
     * 获取星座 time格式为pattern
     *
     * @param time    时间字符串
     * @param pattern 时间格式
     * @return 生肖
     */
    public static String getZodiac(String time, String pattern) {
        return getZodiac(string2Date(time, pattern));
    }

    /**
     * 获取星座
     *
     * @param date Date类型时间
     * @return 星座
     */
    public static String getZodiac(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH) + 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);
        return getZodiac(month, day);
    }

    /**
     * 获取星座
     *
     * @param millis 毫秒时间戳
     * @return 星座
     */
    public static String getZodiac(long millis) {
        return getZodiac(millis2Date(millis));
    }

    /**
     * 获取星座
     *
     * @param month 月
     * @param day   日
     * @return 星座
     */
    public static String getZodiac(int month, int day) {
        return ZODIAC[day >= ZODIAC_FLAGS[month - 1] ? month - 1 : (month + 10) % 12];
    }

    /**
     * 时间差计算
     *
     * @param time 当前时间
     * @return string 0天0时11分55秒
     */
    public static String parseMillisecone(Date time) {

        return parseMillisecone(countTimeDiff(date2Millis(time)));
    }

    /**
     * 时间差计算
     *
     * @param time 当前时间
     * @return string 0天0时11分55秒
     */
    public static String parseMillisecone(String time) {

        return parseMillisecone(countTimeDiff(string2Millis(time)));
    }

    /**
     * 时间差计算
     *
     * @param time 当前时间
     * @return string 0天0时11分55秒
     */
    public static String parseMillisecone(String time, String pattern) {

        return parseMillisecone(countTimeDiff(string2Millis(time, pattern)));
    }

    /**
     * 时间差计算
     *
     * @param millisecond
     * @return string 0天0时11分55秒
     */
    public static String parseMillisecone(long millisecond) {
        String time = null;
        try {
            long yushu_day = millisecond % DAY;
            long yushu_hour = millisecond % (DAY * HOUR);
            long yushu_minute = millisecond % (DAY * HOUR * MINUTE);
            @SuppressWarnings("unused") long yushu_second = millisecond % (DAY * HOUR * MINUTE * SECOND);
            if (yushu_day == 0) {
                return (millisecond / DAY) + "天";
            } else {
                if (yushu_hour == 0) {
                    return (millisecond / DAY) + "天" + (yushu_day / HOUR) + "时";
                } else {
                    if (yushu_minute == 0) {
                        return (millisecond / DAY) + "天" + (yushu_day / HOUR) + "时" + (yushu_hour / MINUTE) + "分";
                    } else {
                        return (millisecond / DAY) + "天" + (yushu_day / HOUR) + "时" + (yushu_hour / MINUTE) + "分" + (yushu_minute / SECOND) + "秒";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    /**
     * 计算两个时间的时间差
     *
     * @param first  时间
     * @param second 时间
     * @return int
     * @author LL
     * @date 2018年8月2日 下午5:49:29
     */
    public static int longOfTwoDate(Date first, Date second) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(first);
        int cnt = 0;
        while (calendar.getTime().compareTo(second) != 0) {
            calendar.add(Calendar.DATE, 1);
            cnt++;
        }
        return cnt;
    }

    /**
     * 计算两个时间的时间差
     *
     * @param first  时间
     * @param second 时间
     * @return int
     * @author LL
     * @date 2018年8月2日 下午5:49:29
     */
    public static int longOfTwoDate(String first, String second, String pattern) {

        Date firstDate = string2Date(first, pattern);
        Date secondDate = string2Date(second, pattern);
        return longOfTwoDate(firstDate, secondDate);
    }

    /**
     * 时间差计算
     *
     * @param time 当前时间
     * @return string 0天0时11分55秒
     */
    public static long getDifference(Date time, int f) {

        return getDifference(countTimeDiff(date2Millis(time)), f);
    }

    /**
     * 时间差计算
     *
     * @param time 当前时间
     * @return string 0天0时11分55秒
     */
    public static long getDifference(String time, int f) {

        return getDifference(countTimeDiff(string2Millis(time)), f);
    }

    /**
     * 时间差计算
     *
     * @param time 当前时间
     * @return string 0天0时11分55秒
     */
    public static long getDifference(String time, String pattern, int f) {

        return getDifference(countTimeDiff(string2Millis(time, pattern)), f);
    }

    /**
     * 比较两个时间的大小
     *
     * @param
     * @return int 1:前大于后;-1:前小于后;0相等
     * @author LL
     * @date 2018年7月6日 上午10:25:52
     */
    public static int compareDate(Date date1, Date date2) {

        if (date1 == null || date2 == null) {
            throw new NullPointerException();
        }
        return compareDate(date1.getTime(), date2.getTime());
    }

    /**
     * 比较两个时间的大小
     *
     * @param
     * @return int 1:前大于后;-1:前小于后;0相等
     * @author LL
     * @date 2018年7月6日 上午10:25:52
     */
    public static int compareDate(long date1, long date2) {
        if (date1 > date2) {
            return 1;
        } else if (date1 < date2) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * 比较两个时间的大小
     *
     * @param
     * @return int 1:前大于后;-1:前小于后;0相等
     * @author LL
     * @date 2018年7月6日 上午10:25:52
     */
    public static int compareDate(String date1, String date2, String pattern) {

        long longDate1 = string2Millis(date1, pattern);
        long longDate2 = string2Millis(date2, pattern);
        return compareDate(longDate1, longDate2);
    }

    /**
     * 计算时间与当前时间相差多久
     *
     * @param time
     * @param f    时间差的形式0:秒,1:分种,2:小时,3:天
     * @return 返回int类型长度，多出则+1
     */
    public static long getDifference(long time, int f) {
        long result = 0;
        long yushu_day = time % DAY;
        long yushu_hour = time % (DAY * HOUR);
        long yushu_minute = time % (DAY * HOUR * MINUTE);
        long yushu_second = time % (DAY * HOUR * MINUTE * SECOND);
        try {
            // 日期相减获取日期差X(单位:毫秒)
            long millisecond = System.currentTimeMillis() - time;
            /**
             * Math.abs((int)(millisecond/1000)); 绝对值 1秒 = 1000毫秒
             * millisecond/1000 --> 秒 millisecond/1000*60 - > 分钟
             * millisecond/(1000*60*60) -- > 小时 millisecond/(1000*60*60*24) -->
             * 天
             */
            switch (f) {
                case 0: {
                    if (yushu_second != 0) {
                        return (millisecond / SECOND) + 1;
                    }
                    return (millisecond / SECOND);
                }
                case 1: {
                    if (yushu_minute != 0) {
                        return (millisecond / MINUTE) + 1;
                    }
                    return (millisecond / MINUTE);
                }
                case 2: {
                    if (yushu_hour != 0) {
                        return (millisecond / HOUR) + 1;
                    }
                    return (millisecond / HOUR);
                } // hour
                case 3: {
                    if (yushu_day != 0) {
                        return (millisecond / DAY) + 1;
                    }
                    return (millisecond / DAY);
                } // day
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 计算时间差
     *
     * @param time 时间戳
     * @return 时间差的时间戳
     */
    public static long countTimeDiff(long time) {

        return System.currentTimeMillis() - time;
    }

    /**
     * 获取当前时间的前一天 转换成字符串
     *
     * @param
     * @return Date
     * @description
     * @author dw
     * @since 2018年7月9日 下午4:14:32
     */
    public static String getNextDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        date = calendar.getTime();
        String string = DEFAULT_SIMPLE_SDF.format(date);
        return string;
    }

    /**
     * 获取当前时间的前30天
     *
     * @param
     * @return Date
     * @description
     * @author dw
     * @since 2018年7月9日 下午4:14:32
     */
    public static String getNextSanDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -30);
        date = calendar.getTime();
        String string = DEFAULT_SIMPLE_SDF.format(date);
        return string;
    }

    /**
     * 获取当前时间的前一天
     *
     * @param
     * @return Date
     * @description
     * @author dw
     * @since 2018年7月9日 下午4:14:32
     */
    public static Date getNextDayDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        date = calendar.getTime();
        return date;
    }


    /**
     * 获取最近一周的所有日期
     *
     * @return java.util.List<java.lang.String>
     * @author mengwei
     * @date 2021/6/7 14:02
     */
    public static List<String> getNearlyWeekDates() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        //过去七天
        c.setTime(new Date());
        String today = format.format(new Date());
        c.add(Calendar.DATE, -7);
        Date d = c.getTime();
        String day = format.format(d);
        List<String> result = getBetweenDates(day, today, false);
        return result;
    }

    /**
     * 补全给定起止时间区间内的所有日期
     *
     * @param startTime          开始日期
     * @param endTime            截止日期
     * @param isIncludeStartTime 是否包含开始日期
     * @return java.util.List<java.lang.String>
     * @author mengwei
     * @date 2021/6/7 14:01
     */
    public static List<String> getBetweenDates(String startTime, String endTime, boolean isIncludeStartTime) {
        List<String> result = new ArrayList<>();
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date d1 = new SimpleDateFormat("yyyy-MM-dd").parse(startTime);//定义起始日期
            Date d2 = new SimpleDateFormat("yyyy-MM-dd").parse(endTime);//定义结束日期  可以去当前月也可以手动写日期。
            Calendar dd = Calendar.getInstance();//定义日期实例
            dd.setTime(d1);//设置日期起始时间
            if (isIncludeStartTime) {
                result.add(format.format(d1));
            }
            while (dd.getTime().before(d2)) {//判断是否到结束日期
                dd.add(Calendar.DATE, 1);//进行当前日期加1
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String str = sdf.format(dd.getTime());
                result.add(str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 最近一个月的所有日期
     *
     * @return java.util.List<java.lang.String>
     * @author mengwei
     * @date 2021/6/9 10:05
     */
    public static List<String> getNearlyMonthDates() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        //过去一月
        c.setTime(new Date());
        String today = format.format(new Date());
        c.add(Calendar.MONTH, -1);
        Date m = c.getTime();
        String mon = format.format(m);
        List<String> result = getBetweenDates(mon, today, false);

        return result;
    }

    /**
     * 最近一年的所有月份
     *
     * @return java.util.List<java.lang.String>
     * @author mengwei
     * @date 2021/6/9 10:05
     */
    public static List<String> getNearlyYearDates() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        //过去一年
        c.setTime(new Date());
        String today = format.format(new Date());
        c.add(Calendar.MONTH, -11);
        Date y = c.getTime();
        String year = format.format(y);
        //如果要获取近一年内的所有日期则，调用getBetweenDates()方法
        List<String> result = getMonthsBetweenDates(year, today);

        return result;
    }

    /**
     * 最近6个月的所有月份
     *
     * @return java.util.List<java.lang.String>
     * @author y j c
     * @date 2022/1/4 10:05
     */
    public static List<String> getNearly6MonthsDates() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        //过去一年
        c.setTime(new Date());
        String today = format.format(new Date());
        c.add(Calendar.MONTH, -5);
        Date y = c.getTime();
        String year = format.format(y);
        //如果要获取近一年内的所有日期则，调用getBetweenDates()方法
        List<String> result = getMonthsBetweenDates(year, today);
        return result;
    }


    /**
     * 补全给定时间区内的所有月份
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return java.util.List<java.lang.String>
     * @author mengwei
     * @date 2021/6/9 10:08
     */
    public static List<String> getMonthsBetweenDates(String startTime, String endTime) {
        List<String> result = new ArrayList<>();
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
            Date d1 = new SimpleDateFormat("yyyy-MM").parse(startTime);//定义起始日期
            Date d2 = new SimpleDateFormat("yyyy-MM").parse(endTime);//定义结束日期  可以去当前月也可以手动写日期。
            Calendar dd = Calendar.getInstance();//定义日期实例
            dd.setTime(d1);//设置日期起始时间
            result.add(format.format(d1));
            while (dd.getTime().before(d2)) {//判断是否到结束日期
                dd.add(Calendar.MONTH, 1);//进行当前日期月份加1
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
                String str = sdf.format(dd.getTime());
                result.add(str);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 补全给定时间内的所有周，包含最开始的
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return java.util.ArrayList<java.lang.String>
     * @author mengwei
     * @date 2021/6/9 10:08
     */
    public static ArrayList<String> getWeeksBetweenDates(String startTime, String endTime) {
        ArrayList<String> result = new ArrayList<>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date start = null;
        Date end = null;
        try {
            start = format.parse(startTime);
            end = format.parse(endTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setFirstDayOfWeek(Calendar.MONDAY);
            calendar.setTime(start);
            result.add(calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.WEEK_OF_YEAR));
            while (calendar.getTime().before(end)) {
                calendar.add(Calendar.WEEK_OF_YEAR, 1);
                String weekStr = calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.WEEK_OF_YEAR);
                result.add(weekStr);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 获得该月第一天
     *
     * @param year  年
     * @param month 月
     * @return java.lang.String
     * @author mengwei
     * @date 2021/6/9 10:27
     */
    public static String getFirstDayOfMonth(int year, int month) {
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR, year);
        //设置月份
        cal.set(Calendar.MONTH, month - 1);
        //获取某月最小天数
        int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
        //设置日历中月份的最小天数
        cal.set(Calendar.DAY_OF_MONTH, firstDay);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(cal.getTime());
    }

    /**
     * 获得该月最后一天
     *
     * @param year  年
     * @param month 月
     * @return java.lang.String
     * @author mengwei
     * @date 2021/6/9 10:28
     */
    public static String getLastDayOfMonth(int year, int month) {
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR, year);
        //设置月份
        cal.set(Calendar.MONTH, month - 1);
        //获取某月最大天数
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        //设置日历中月份的最大天数
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        //格式化日期
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(cal.getTime());
    }

    /**
     * 获得该月最后一天
     *
     * @param year  年
     * @param month 月
     * @return java.lang.String
     * @author mengwei
     * @date 2021/6/9 10:28
     */
    public static int getLastDay(int year, int month) {
        Calendar cal = Calendar.getInstance();
        //设置年份
        cal.set(Calendar.YEAR, year);
        //设置月份
        cal.set(Calendar.MONTH, month - 1);
        //获取某月最大天数
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);

        return lastDay;
    }

    /**
     * @description: 日期格式  yy-MM-dd转换为 yyyyMMdd
     * @param: date
     * @return:
     * @author 张佳宁
     * @date: 2023/11/15 3:44 PM
     */
    public static String getYyyyDdMmByStringDay(String date) throws ParseException {
        if (date == null) {
            date = date3String(new Date());
        }
        Date format2 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        String shortDate = new SimpleDateFormat("yyyyMMdd").format(format2);
        return shortDate;
    }

    /**
     * @方法目的: 获取指定天数之后的日期
     * @创建人员: Jn
     * @创建时间: 2021/12/24 10:47
     * @方法逻辑: params(date : 当前时间, day : 多少天之后)
     */
    public static Date getAfterDayDate(Date date, Integer day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, day);
        date = calendar.getTime();
        return date;
    }

    public static int daysBetween(Date first, Date second) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(first);
        long firstTime = calendar.getTimeInMillis();
        calendar.setTime(second);
        long secondTime = calendar.getTimeInMillis();
        long between = (secondTime - firstTime) / (1000 * 3600 * 24);
        return Integer.parseInt(String.valueOf(between));
    }

    /**
     * @param date     开始时间
     * @param timeSolt 时间段，分钟
     * @author y j c
     * @description 开始时间+时间段=结束时间(之前时间传负数)
     * @date 2022/11/13 22:08
     * @return: 开始时间+时间段
     **/
    public static Date getBeforeOrAfterDate(Date date, Integer timeSolt) {
        if (null == date || null == timeSolt) {
            return null;
        }
        Date beforeDate = null;
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MINUTE, timeSolt);
            beforeDate = calendar.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return beforeDate;
    }


    /**
     * 分开 获取 年 月 日
     **/
    public static Integer getDayOjn() {

//        Calendar calendar = Calendar.getInstance();
//
//        int year = calendar.get(Calendar.YEAR);
//
//        int month = calendar.get(Calendar.MONTH);
//
//        int date = calendar.get(Calendar.DATE);
//
//        int hour = calendar.get(Calendar.HOUR_OF_DAY);
//
//        int minute = calendar.get(Calendar.MINUTE);
//
//        int second = calendar.get(Calendar.SECOND);

        return Calendar.getInstance().get(Calendar.DATE);
    }

    public static void main(String[] args) {
        System.out.println(getNowTimeDateString());
    }


}
