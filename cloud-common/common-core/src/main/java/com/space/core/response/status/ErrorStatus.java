package com.space.core.response.status;


public enum ErrorStatus {

    /**
     * 参数非法
     */
    PARAMETER_ERROR("S10001", "参数非法"),
    /**
     * 文件服务器异常
     */
    FASTDFS_ERROR("S10004", "文件服务器异常"),
    /**
     * 图片太大或者太小
     */
    PIC_SIZE_ERROR("S10005", "图片大小不符合"),
    /**
     * 流程报错
     */
    MANAGER_ERROR("S10006", "流程报错"),
    /**
     * 不支持该文件类型
     */
    FILE_EXTENSION_NOT_SUPPORT("F10001", "不支持该文件类型"),

    ;

    private String status;

    private String message;

    private ErrorStatus(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public String status() {
        return status;
    }

    public String message() {
        return message;
    }
}
