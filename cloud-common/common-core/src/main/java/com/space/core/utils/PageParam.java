package com.space.core.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import java.io.Serializable;



@Data
public class PageParam implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long pageSize = 0L;

    private Long pageNum = 10L;

    public Page toPage() {
        //mybatis-plus分页
        return new Page(this.pageNum, this.pageSize);
    }
}
